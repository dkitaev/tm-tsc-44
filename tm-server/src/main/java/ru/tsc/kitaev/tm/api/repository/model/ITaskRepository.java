package ru.tsc.kitaev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(@NotNull final Task task);

    void update(@NotNull final Task task);

    void clear();

    void clearByUserId(@NotNull final String userId);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAllByUserId(@NotNull final String userId);

    @NotNull
    Task findById(@NotNull final String userId, @NotNull final String id);

    @NotNull
    Task findByIndex(@NotNull final String userId, @NotNull final Integer index);

    void removeById(@NotNull final String userId, @NotNull final String id);

    void removeByIndex(@NotNull final String userId, @NotNull final Integer index);

    @NotNull
    Integer getSize(@NotNull final String userId);

    @NotNull
    Task findByName(@NotNull final String userId, @NotNull final String name);

    void removeByName(@NotNull final String userId, @NotNull final String name);

    @NotNull
    List<Task> findAllTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    void removeAllTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

}
