package ru.tsc.kitaev.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "sessions")
public final class SessionDTO extends AbstractEntityDTO implements Cloneable {

    @NotNull
    @Column(name = "user_id")
    private String userId;

    @Nullable
    @Column
    private String signature;

    @NotNull
    @Column(name = "time_stamp")
    private Long timestamp;

    public SessionDTO(@NotNull final String userId) {
        this.userId = userId;
    }

    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
