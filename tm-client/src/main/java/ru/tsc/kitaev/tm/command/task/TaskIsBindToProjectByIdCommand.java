package ru.tsc.kitaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.endpoint.SessionDTO;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class TaskIsBindToProjectByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "tasks-bind-to-project-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Bind task to project by id...";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectEndpoint().findProjectById(session, projectId) == null) throw new ProjectNotFoundException();
        System.out.println("Enter task id");
        @NotNull final String taskId = TerminalUtil.nextLine();
        if (serviceLocator.getTaskEndpoint().findTaskById(session, taskId) == null) throw new TaskNotFoundException();
        serviceLocator.getProjectTaskEndpoint().bindTaskById(session, projectId, taskId);
    }

}
